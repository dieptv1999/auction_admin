import { notification } from 'antd';
import _ from 'lodash';
import consts from '../consts';
import sha1 from 'js-sha1';

// let timeoutID;
const showNotification = (message = 'title', description = 'description', type = 'success') => {
  notification[type]({
    message,
    description,
  });
};

// const clone = (obj) => Object.assign({}, obj);

const renameKey = (object, key, newKey) => {
  const clonedObj = { ...object };
  const targetKey = clonedObj[key];
  delete clonedObj[key];
  clonedObj[newKey] = targetKey;
  return clonedObj;
};

const handleVietnamese = (str) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  // Some system encode vietnamese combining accent as individual utf-8 characters
  // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
  // Remove extra spaces
  // Bỏ các khoảng trắng liền nhau
  str = str.replace(/ + /g, ' ');
  str = str.trim();
  // Remove punctuations
  // Bỏ dấu câu, kí tự đặc biệt
  str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, ' ');
  return str;
};

const addHeader = (data, header) => {
  // header = header.map(col => window._.startCase(window._.toLower(col)));
  data.unshift(header);
  return data;
};

const trimAll = (obj) => {
  if (window._.isArray(obj)) {
    console.log('please pass object here! src/common/utils.js:29');
    return;
  }

  window._.forEach(obj, (value, key) => {
    if (typeof obj[`${key}`] === 'object') {
      trimAll(obj[`${key}`]);
    } else {
      if (key !== `${key}`.trim()) {
        delete obj[`${key}`];
        key = `${key}`.trim();
      }
      if (typeof value === 'string') {
        obj[`${key}`] = window._.trim(value);
      }
    }
  });
};

const paddingZeros = (num, count = 19) => ('0'.repeat(count) + num).substr(-count, count);
const trimRightZero = (num) => (`${num}`.split('.').length === 2 ? window._.trimEnd(num, '0') : `${num}`);
const trimDot = (num) => window._.trimEnd(`${num}`, '.');
const trimRightZeroAndDot = (num) => trimDot(trimRightZero(num));

const fillTable = (arr) => {
let newArr = arr;
if(_.isArray(newArr)){
  while(newArr?.length % consts.DEFAULT_PAGE_SIZE !== 0){
    newArr = [...newArr, {}]
  }
  return newArr
}
}

function hash(object) {
  return sha1(JSON.stringify(object));
}

function getCoinName(coins, symbol) {
  return window._.find(coins, ['symbol', symbol]);
}

function resetCacheTimeTVChart() {
  sessionStorage.setItem('currentTimeRequest', '');
  sessionStorage.setItem('previousTimeRequest', '');
}

function upperCaseFirst(input) {
  return window._.upperFirst(input);
}

/**
 * Check if string is HEX, requires a 0x in front
 *
 * @method isHexStrict
 * @param {String} hex to be checked
 * @returns {Boolean}
 */
const isHexStrict = function (hex) {
  return ((_.isString(hex) || _.isNumber(hex)) && /^(-)?0x[0-9a-f]*$/i.test(hex));
};

/**
 * Convert a hex string to a byte array
 *
 * Note: Implementation from crypto-js
 *
 * @method hexToBytes
 * @param {string} hex
 * @return {Array} the byte array
 */
const hexToBytes = function (hex) {
  let truncateHex = hex.toString(16);

  if (!isHexStrict(hex)) {
    throw new Error(`Given value "${hex}" is not a valid hex string.`);
  }

  truncateHex = truncateHex.replace(/^0x/i, '');
  const bytes = [];
  for (let c = 0; c < truncateHex.length; c += 2) {
    bytes.push(parseInt(truncateHex.substr(c, 2), 16));
  }

  return bytes;
};

export default {
  resetCacheTimeTVChart,
  hash,
  trimAll,
  showNotification,
  paddingZeros,
  trimRightZero,
  trimRightZeroAndDot,
  upperCaseFirst,
  renameKey,
  handleVietnamese,
  fillTable,
};
