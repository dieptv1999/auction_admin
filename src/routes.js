import React from "react";
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import BubbleChart from "@material-ui/icons/BubbleChart";
import StoreMallDirectoryIcon from '@material-ui/icons/StoreMallDirectory';
import {faGavel, faAward} from "@fortawesome/free-solid-svg-icons";
import DashboardPage from "./containers/DashBoardContainer";
import Product from "./containers/ProductContainer";
import UserProfile from "./views/UserProfile/UserProfile";
import Auctions from "./views/Auctions";
import Winners from "./views/Winners";
import SubCC from "./views/SubCC";

const dashboardRoutes = [
    {
        path: "/dashboard",
        name: "Bảng điều khiển",
        rtlName: "Bảng điều khiển",
        icon: Dashboard,
        typeIcon: 'material',
        component: (props, data) => (<DashboardPage {...props}/>),
        layout: "/cms"
    },
    {
        path: "/user",
        name: "Thông tin cá nhân",
        rtlName: "Thông tin cá nhân",
        icon: Person,
        component: (props, data) => (<UserProfile {...props}/>),
        layout: "/cms"
    },
    {
        path: "/product",
        name: "Quản lý sản phẩm",
        rtlName: "Quản lý sản phẩm",
        icon: StoreMallDirectoryIcon,
        typeIcon: 'material',
        component: (props, data) => (<Product {...props} data={data}/>),
        layout: "/cms"
    },
    {
        path: "/auction",
        name: "Quản lý phiên đấu giá",
        rtlName: "Quản lý phiên đấu giá",
        icon: faGavel,
        typeIcon: 'fontawesome',
        component: (props, data) => (<Auctions {...props} data={data}/>),
        layout: "/cms"
    },
    {
        path: "/winners",
        name: "Quản lý người trúng thưởng",
        rtlName: "Quản lý người trúng thưởng",
        icon: faAward,
        typeIcon: 'fontawesome',
        component: (props, data) => (<Winners {...props}/>),
        layout: "/cms"
    },
    {
        path: "/sub_cc",
        name: "Tra cứu sub/CC",
        rtlName: "Tra cứu sub/CC",
        icon: BubbleChart,
        typeIcon: 'material',
        component: (props, data) => (<SubCC {...props}/>),
        layout: "/cms"
    },
];

export default dashboardRoutes;
