import React, {useEffect} from 'react';
import CardFooter from "../components/Card/CardFooter";
import Button from "@material-ui/core/Button";
import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";
import Card from "../components/Card/Card";
import CardHeader from "../components/Card/CardHeader";
import {notification} from "antd";
import axios from "axios";
import CardBody from "../components/Card/CardBody";
import CustomInput from "../components/CustomInput/CustomInput";
import {InputAdornment} from "@material-ui/core";
import {Email} from "@material-ui/icons";
import makeStyles from "@material-ui/core/styles/makeStyles";
import styles from 'assets/jss/cms/views/loginStyles.js';
import Icon from "@material-ui/core/Icon";

const useStyles = makeStyles(styles);

function Login(props) {
    const classes = useStyles();
    const onSubmit = async event => {
        event.preventDefault();
        const {email, password} = event.target
        console.log(email.value, password.value)
        loginNormal(email.value, password.value)
    }

    const authStep1 = () => {
        props.onLoginAction({}, () => {
            props.history.push('/')
        })
    }

    const loginNormal = (email, password) => {
        props.onLoginNormal({
            email,
            password
        }, () => {
            props.history.push('/')
        })
    }

    useEffect(() => {
        authStep1()
    }, [])

    return (
        <div className={classes.section}>
            <div className={classes.container}>
                <GridContainer justify="center">
                    <GridItem xs={12} sm={6} md={4}>
                        <Card>
                            <form className={classes.form} onSubmit={onSubmit}>
                                <CardHeader color="primary" className={classes.cardHeader}>
                                    <h4 className={classes.loginTitle}>Login</h4>
                                </CardHeader>
                                <CardBody>
                                    <CustomInput
                                        labelText="Email..."
                                        id="email"
                                        name={"email"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            type: "email",
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <Email className={classes.inputIconsColor}/>
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                    <CustomInput
                                        labelText="Password"
                                        id="pass"
                                        name={'password'}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            type: "password",
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <Icon className={classes.inputIconsColor}>
                                                        lock_outline
                                                    </Icon>
                                                </InputAdornment>
                                            ),
                                            autoComplete: "off"
                                        }}
                                    />
                                </CardBody>
                                <CardFooter className={classes.cardFooter}>
                                    <Button simple color="primary" size="lg" type={"submit"}>
                                        Get Started
                                    </Button>
                                </CardFooter>
                            </form>
                        </Card>
                    </GridItem>
                </GridContainer>
            </div>
        </div>
    )
}

export default Login;
