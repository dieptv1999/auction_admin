import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";

import routes from "routes.js";

import styles from "assets/jss/cms/layouts/adminStyle.js";

import bgImage from "assets/img/sidebar-2.jpg";
import logo from "assets/img/reactlogo.png";
import {faGavel} from "@fortawesome/free-solid-svg-icons";

let ps;

const switchRoutes = (data = 1) => (
    <Switch>
        {routes.map((prop, key) => {
            if (prop.layout === "/cms") {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        render={(props) => prop.component(props, data)}
                        key={key}
                    />
                );
            }
            return null;
        })}
        <Redirect
            from="/cms"
            to="/cms/dashboard"/>
    </Switch>
);

const useStyles = makeStyles(styles);

export default function Admin({...rest}) {
    const classes = useStyles();
    const mainPanel = React.createRef();
    const [image, setImage] = React.useState(bgImage);
    const [color, setColor] = React.useState("blue");
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [searching, setSearching] = React.useState({})
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const getRoute = () => {
        return window.location.pathname !== "/cms/maps";
    };
    const resizeFunction = () => {
        if (window.innerWidth >= 960) {
            setMobileOpen(false);
        }
    };
    React.useEffect(() => {
        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(mainPanel.current, {
                suppressScrollX: true,
                suppressScrollY: false
            });
            document.body.style.overflow = "hidden";
        }
        window.addEventListener("resize", resizeFunction);
        // Specify how to clean up after this effect:
        return function cleanup() {
            if (navigator.platform.indexOf("Win") > -1) {
                ps.destroy();
            }
            window.removeEventListener("resize", resizeFunction);
        };
    }, [mainPanel]);


    const search = (input) => {
        setSearching({input})
    }
    return (
        <div className={classes.wrapper}>
            <Sidebar
                routes={routes}
                logoText={"Auction"}
                logo={faGavel}
                image={image}
                handleDrawerToggle={handleDrawerToggle}
                open={mobileOpen}
                color={color}
                {...rest}
            />
            <div className={classes.mainPanel} ref={mainPanel}>
                <Navbar
                    routes={routes}
                    handleDrawerToggle={handleDrawerToggle}
                    {...rest}
                    callback={search}
                />
                {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
                {getRoute() ? (
                    <div className={classes.content}>
                        <div className={classes.container}>{switchRoutes(searching)}</div>
                    </div>
                ) : (
                    <div className={classes.map}>{switchRoutes(searching)}</div>
                )}
                {getRoute() ? <Footer/> : null}
                {/*<FixedPlugin*/}
                {/*  handleImageClick={handleImageClick}*/}
                {/*  handleColorClick={handleColorClick}*/}
                {/*  bgColor={color}*/}
                {/*  bgImage={image}*/}
                {/*  handleFixedClick={handleFixedClick}*/}
                {/*  fixedClasses={fixedClasses}*/}
                {/*/>*/}
            </div>
        </div>
    );
}
