import React, {useState} from 'react';
import actions from './redux/actions/user';
import "assets/css/material-dashboard-react.css?v=1.9.0";
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import {Redirect, Route, Router, Switch, BrowserRouter} from "react-router-dom";
import Admin from "./layouts/Admin";
import Login from "./containers/LoginContainer";
import RTL from "./layouts/RTL";
import {createBrowserHistory} from "history";
import {withRouter} from "react-router";

const hist = createBrowserHistory();

function App() {

    return (
        <>
            {true ? <Router history={hist}>
                <Switch>
                    <Route path="/cms" component={Admin}/>
                    <Route path="/rtl" component={RTL}/>
                    <Redirect from="/" to="/cms/dashboard"/>
                </Switch>
            </Router> : <BrowserRouter><Login/></BrowserRouter>}
        </>
    );
}

const mapStateToProps = (state) => ({
    user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
    logout(params) {
        dispatch(actions.logout(params));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
