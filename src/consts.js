// export const BASE_URL = 'http://10.120.1.11:9082';
// export const BASE_URL = 'https://admin.crypto.mobilelab.vn';
export const BASE_URL = 'http://127.0.0.1:9088';
// export const BASE_URL = 'http://192.168.100.11:9082';
// export const BASE_URL = 'http://localhost:9082';
// export const BASE_URL = 'http://113.190.221.206:9082';
// export const PREFIX_FRONT_URL = "";
export const PREFIX_FRONT_URL = '';
export const API_VERSION = 'v1';
export const TIME_OF_DEBOUNCE = 300;
const SC_CONST = {
  ORDER_HISTORY: '/order/orders',
  ORDER_TRANS_HISTORY: '/order-transaction/all',
  WITHDRAW_HISTORY: '/transaction/history',

  LIST_USER: '/user/paginate',
  LOGIN_HISTORY: '/user/login-histories',

  SETTING_COIN: '/settings/coin',
  SETTING_FEE_TRANS: '/settings/transaction-fee',
  TRANS_FEE: '/settings/order-fee',

  TOOL_TRACE_ADDRESS: '/tool/trace-address',

  ADMIN_MANAGEMENT: '/admin/all-admins',
  ROLE_MANAGEMENT: '/authorization/roles',
  PERM_MANAGEMENT: '/authorization/permissions',
  PERM_ACTION_MANAGEMENT: '/authorization/controls',

  STATIS_ORDER_TRANS: '/statistic/inout-order-trans',
  STATIS_INOUT_TRANS: '/statistic/inout-trans',
  STATIS_INOUT_TRANS_EXTERNAL: '/statistic/inout-trans-external',
  STATIS_WALLET_BALANCE: '/statistic/wallet-balance',
};

const CONTROLS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

const LangDateVi = {
  lang: {
    locale: 'vi_VN',
    placeholder: 'Chọn ngày',
    rangePlaceholder: ['Ngày bắt đầu', 'Ngày kết thúc'],
    today: 'Hôm nay',
    now: 'Bây giờ',
    backToToday: 'Quay lại hôm nay',
    ok: 'Đồng ý',
    clear: 'Làm mới',
    month: 'Tháng',
    year: 'Năm',
    timeSelect: 'Chọn thời gian',
    dateSelect: 'Chọn ngày',
    monthSelect: 'Chọn tháng',
    yearSelect: 'Chọn năm',
    decadeSelect: 'Chọn thập kỷ',
    yearFormat: 'YYYY',
    dateFormat: 'M/D/YYYY',
    dayFormat: 'D',
    dateTimeFormat: 'M/D/YYYY HH:mm:ss',
    monthFormat: 'MMMM',
    monthBeforeYear: true,
    previousMonth: 'Tháng trước (Trang trên)',
    nextMonth: 'Tháng sau (Trang dưới)',
    previousYear: 'Năm ngoái (Control + trái)',
    nextYear: 'Năm sau (Control + phải)',
    previousDecade: 'Thập kỷ vừa qua',
    nextDecade: 'Thập kỷ tới',
    previousCentury: 'Thế kỷ trước',
    nextCentury: 'Thế kỷ tới',
  },
  timePickerLocale: {
    placeholder: 'Chọn thời gian',
  },
  dateFormat: 'YYYY-MM-DD',
  dateTimeFormat: 'YYYY-MM-DD HH:mm:ss',
  weekFormat: 'YYYY-wo',
  monthFormat: 'YYYY-MM',
};

export default {
  // ============ RBAC ============
  // todo define all controls in screen here...
  LangDateVi,
  CONTROLS,
  MAP_CRYPTOS: {
    [SC_CONST.ORDER_HISTORY]: CONTROLS,
    [SC_CONST.ORDER_TRANS_HISTORY]: CONTROLS,
    [SC_CONST.WITHDRAW_HISTORY]: CONTROLS,
    [SC_CONST.LIST_USER]: CONTROLS,
    [SC_CONST.LOGIN_HISTORY]: CONTROLS,
    [SC_CONST.SETTING_COIN]: CONTROLS,
    [SC_CONST.SETTING_FEE_TRANS]: CONTROLS,
    [SC_CONST.TRANS_FEE]: CONTROLS,
    [SC_CONST.TOOL_TRACE_ADDRESS]: CONTROLS,
    [SC_CONST.ADMIN_MANAGEMENT]: CONTROLS,
    [SC_CONST.ROLE_MANAGEMENT]: CONTROLS,
    [SC_CONST.PERM_MANAGEMENT]: CONTROLS,
    [SC_CONST.PERM_ACTION_MANAGEMENT]: CONTROLS,
    [SC_CONST.STATIS_ORDER_TRANS]: CONTROLS,
    [SC_CONST.STATIS_INOUT_TRANS]: CONTROLS,
    [SC_CONST.STATIS_INOUT_TRANS_EXTERNAL]: CONTROLS,
    [SC_CONST.STATIS_WALLET_BALANCE]: CONTROLS,
  },

  // for RBAC
  ALL_NAMESPACES: [
    '/admin',
    '/adminnotification',
    '/affiliate-admin',
    '/authorization/controls',
    '/authorization/permissions',
    '/authorization/role-permissions',
    '/authorization/role-perms-controls',
    '/authorization/role-users',
    '/authorization/roles',
    '/banner',
    '/exchange',
    '/order-transaction',
    '/order',
    '/setting',
    '/statistic',
    '/tools',
    '/transaction',
    '/user',
    '/campaign',
    '/commission',
  ],

  // ============ RBAC ============

  TYPE_ERROR: 'error',
  TYPE_WARNING: 'warning',
  TYPE_SUCCESS: 'success',
  TYPE_INFO: 'info',
  FORM_LAYOUT: {
    HORIZONTAL: 'horizontal',
    VERTICAL: 'vertical',
    INLINE: 'inline',
  },
  MODAL_CONFIG_PREFIX: '',
  MODAL_DETAIL: 'detail',
  MODAL_EDIT: 'edit',
  MODAL_DEL: 'del',
  MODAL_ADD: 'add',
  MODAL_DOWNLOAD: 'download',
  MODAL_API_GROUP: 'api_group',
  MODAL_CONFIG_DETAIL: 'config_detail',
  MODAL_CONFIG_EDIT: 'config_edit',
  MODAL_CONFIG_CLONE: 'config_clone',
  MODAL_CONFIG_DEL: 'config_del',
  MODAL_CONFIG_ADD: 'config_add',
  MODAL_CONFIG_DOWNLOAD: 'config_download',
  MODAL_API_GROUP_DETAIL: 'api_group_detail',
  MODAL_API_GROUP_EDIT: 'api_group_edit',
  MODAL_API_GROUP_DEL: 'api_group_del',
  MODAL_API_GROUP_ADD: 'api_group_add',
  MODAL_API_GROUP_DOWNLOAD: 'api_group_download',
  DEFAULT_HEADERS: {},
  ORDER_TRADE_TYPE_BUY: 0,
  ORDER_TRADE_TYPE_BUY_STR: 'Buy',
  ORDER_TRADE_TYPE_SELL: 1,
  ORDER_TRADE_TYPE_SELL_STR: 'Sell',

  ORDER_TYPE_LIMIT: 0,
  ORDER_TYPE_MARKET: 1,
  ORDER_TYPE_STOP_LIMIT: 2,
  ORDER_TYPE_STOP_MARKET: 3,
  ORDER_TYPE_LIMIT_STR: 'Limit',
  ORDER_TYPE_MARKET_STR: 'Market',
  ORDER_TYPE_STOP_LIMIT_STR: 'Stop Limit',
  ORDER_TYPE_STOP_MARKET_STR: 'Stop Market',

  ORDER_STATUS_STOPPING: 0,
  ORDER_STATUS_PENDING: 1,
  ORDER_STATUS_EXECUTED: 2,
  ORDER_STATUS_CANCELED: 3,
  ORDER_STATUS_EXECUTING: 4,
  ORDER_STATUS_REMOVED: 5,
  ORDER_STATUS_STOPPING_STR: 'Stopping',
  ORDER_STATUS_PENDING_STR: 'Pending',
  ORDER_STATUS_EXECUTED_STR: 'Executed',
  ORDER_STATUS_CANCELED_STR: 'Canceled',
  ORDER_STATUS_EXECUTING_STR: 'Executing',
  ORDER_STATUS_REMOVED_STR: 'Removed',

  TRANSACTION_STATUS_SUCCESS: 0,
  TRANSACTION_STATUS_PENDING: 1,
  TRANSACTION_STATUS_SUBMITTED: 2,
  TRANSACTION_STATUS_ERROR: 3,
  TRANSACTION_STATUS_CANCEL: 4,
  TRANSACTION_STATUS_REJECTED: 5,
  TRANSACTION_STATUS_SUCCESS_STR: 'Succeed',
  TRANSACTION_STATUS_PENDING_STR: 'Pending',
  TRANSACTION_STATUS_SUBMITTED_STR: 'Submitted',
  TRANSACTION_STATUS_ERROR_STR: 'Error',
  TRANSACTION_STATUS_CANCEL_STR: 'Cancel',
  TRANSACTION_STATUS_REJECTED_STR: 'Rejected',

  TRANSACTION_TYPE_DEPOSIT: 0,
  TRANSACTION_TYPE_WITHDRAW: 1,
  TRANSACTION_TYPE_DEPOSIT_STR: 'Trans Deposit',
  TRANSACTION_TYPE_WITHDRAW_STR: 'Trans Withdraw',

  TYPE_ALL: -1,

  DEFAULT_PAGE_SIZE: 10,
  DEFAULT_PAGE_SIZE_OPEN_ORDER: 25,

  PREFIX_FRONT_URL: '/v1/cryptotrading',
  routes: {
    LIST_USERS: 'user/paginate',
    USER_WALLET: 'user/wallet',
    DEPOSIT: 'user/wallet/deposit',
    WITHDRAW: 'user/wallet/withdrawal',
    DEPOSIT_WITHDRAWAL: 'user/wallet/deposit-withdrawal',
    OPEN_ORDER: 'user/wallet/open-order',
    ORDER_HISTORY: 'user/wallet/order-history',
    BUY_WITH_CARD: 'buy-with-card',
    HOME: 'home',
    ROOT: 'root',
    LOGIN: 'login',
    VERIFY: 'verify/email',
    VERIFY_WITHDRAW: 'verify/withdrawal',
    USER_SECURITY: 'user/security',
  },
  style: {
    COMMON_GUTTER: 16,
  },

  DEFAULT_MARGIN: 10,
  DEFAULT_DURATION: 300,
  PRIMARY_COLOR: '#1DA57A',
  COLOR_BUY: '#2EBD85',
  COLOR_SELL: '#E0294A',

  ERC_20: 'erc20',
  ETH: 'eth',

  SYMBOL_ETH: 'ETH',
  SYMBOL_BTC: 'BTC',
  SYMBOL_ERC20: 'ERC20',

  EN: 'en',
  VI: 'vi',
  KO: 'korea',
};
