import BaseRequest from './BaseRequest';

const schema = 'user';
/**
 * key base on host:port
 */
export default class LoginRequest extends BaseRequest {

    /**
     * @param {Object} params
     * @param {string} params.pos
     * @param {string} params.count
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    loginHistories(params) {
        const url = `${schema}/login-histories`;
        return this.get(url, params);
    }

    /**
     * @param {Object} params
     * @param {string} params.pos
     * @param {string} params.count
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    loginNormal(params) {
        const url = `${schema}/login-histories`;
        return this.get(url, params);
    }

    /**
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    logout(params) {
        const url = `${schema}/logout`;
        return this.post(url, params);
    }
}
