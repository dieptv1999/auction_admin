import LoginRequest from './LoginRequest';
import DashBoardRequest from "./DashBoardRequest";
import WinnersRequest from "./WinnersRequest";

const requestMap = {
    LoginRequest,
    DashBoardRequest,
    WinnersRequest,
};

const instances = {};

export default class RequestFactory {
    static getRequest(classname) {
        const RequestClass = requestMap[classname];
        if (!RequestClass) {
            throw new Error(`Invalid request class name: ${classname}`);
        }

        let requestInstance = instances[classname];
        if (!requestInstance) {
            requestInstance = new RequestClass();
            instances[classname] = requestInstance;
        }
        return requestInstance;
    }
}
