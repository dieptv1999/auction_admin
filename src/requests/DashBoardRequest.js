import BaseRequest from './BaseRequest';

const schema = 'dashboard';
/**
 * key base on host:port
 */
export default class DashBoardRequest extends BaseRequest {

    /**
     * @param {Object} params
     * @param {string} params.pos
     * @param {string} params.count
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    loginHistories(params) {
        const url = `${schema}/login-histories`;
        return this.get(url, params);
    }

    /**
     * @param {Object} params
     * @param {string} params.pos
     * @param {string} params.count
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    loginNormal(params) {
        const url = `${schema}/login-histories`;
        return this.get(url, params);
    }
}
