import consts, {API_VERSION, BASE_URL} from "../consts";
import React from "react";
import utils from "../common/utils";
import { CLEAR_USER_DATA } from '../redux/actions/user/action_types';
import _ from 'lodash'

let str = {
  "Withdraw exceed limit": "Số tiền chuyển quá giới hạn",
  "Data Not Existed!": "Không tìm thấy người dùng",

}

let obj = {
  "Contract_Address is required!": "Lối mã giao dịch không được để trống",
  "Image is required!": "Biểu tượng không đươc để trống",
  "Contract_Address is not valid!": "Lối mã giao dịch không tồn tại"
}

export default class BaseRequest {
  version = "v1/cms";


  prefix () {
    return '';
  }

  async get(url, params = {}) {
    try {
      const response = await window.axios.get(`${this.version}/${url}`, { params });
      return this._responseHandler(response);
    } catch (error) {
      this._errorHandler(error);
    }
  }

  async getWithTimeout(url, params = {}, timeout) {
    try {
      const response = await window.axios.get(`${this.version}/${url}`, { params, timeout });
      return this._responseHandler(response);
    } catch (error) {
      this._errorHandler(error);
    }
  }

  async put (url, data = {}) {
    try {
      const response = await window.axios.put(`${this.version}/${url}`, data);
      return this._responseHandler(response);
    } catch (error) {
      this._errorHandler(error);
    }
  }

  async post(url, data = {}) {
    try {
      const response = await window.axios.post(`${this.version}/${url}`, data);
      return this._responseHandler(response);
    } catch (error) {
      this._errorHandler(error);
    }
  }

  async del(url, params = {}) {
    try {
      const response = await window.axios.delete(`${this.version}/${url}`, params);
      return this._responseHandler(response);
    } catch (error) {
      this._errorHandler(error);
    }
  }

  async _responseHandler (response) {
    const {data} = response;
    let errorCode = window._.get(data, 'error.code', 200);

    if (errorCode >= 400) {
      let message = data.error.message;
      let errorsNode = undefined;

      if (typeof(data.error.message) === 'string') {
        errorsNode = <div style={{ fontWeight: 'bold', color: 'red' }}>
          {utils.upperCaseFirst(_.get(str, message) || message)}
        </div>
      } else {
        errorsNode = window._.map(data.error.message, (message, field) => <div style={{ fontWeight: 'bold', color: 'red' }} id={field}>
          {utils.upperCaseFirst(_.get(obj, message) ? _.get(obj, message) : `${field} ${message}`)}
        </div>)
      }

      utils.showNotification(<span style={{ color: 'red', fontWeight: 'bold' }}>Lỗi</span>, errorsNode, consts.TYPE_ERROR);

      if (errorCode === 405) {
        window.h.push("/login");
        window.$dispatch({
          type: CLEAR_USER_DATA
        })
      }
      throw 'Request failed';
    } else if (errorCode >= 300) {
      window.h.push("/")

      throw 'UnAuthorization';
    }

    return data;
  }

  _errorHandler(err) {
    if (err.response && err.response.status === 401) { // Unauthorized (session timeout)
      window.location.href = '/';
    }
    throw err;
  }

  getFile(url) {
    window.location.href = `${BASE_URL}/${API_VERSION}/${url}`;
  }
}
