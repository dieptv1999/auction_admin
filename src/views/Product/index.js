import React, {useState} from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import {faPlusCircle} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Space, Tag, Table, InputNumber, Input, Form, Popconfirm, Typography} from "antd";
import Button from "@material-ui/core/Button";
import Modal from "antd/es/modal/Modal";
import Checkbox from "antd/es/checkbox/Checkbox";


const styles = {
    header_1: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 16
    },
    title: {
        fontSize: 20,
        fontWeight: '700'
    },
    iconAdd: {
        marginRight: 4,
        fontSize: 18
    }
};

const useStyles = makeStyles(styles);

const EditableCell = ({
                          editing,
                          dataIndex,
                          title,
                          inputType,
                          record,
                          index,
                          children,
                          ...restProps
                      }) => {
    const inputNode = inputType === 'number' ? <InputNumber/> : <Input/>;
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{
                        margin: 0,
                    }}
                    rules={[
                        {
                            required: true,
                            message: `Please Input ${title}!`,
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                children
            )}
        </td>
    );
};

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

const originData = [
    {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
        tags: ['nice', 'developer'],
    },
    {
        key: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Joe Black',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
        tags: ['cool', 'teacher'],
    },
];

export default function Product(props) {
    const classes = useStyles();

    const [form] = Form.useForm();
    const [data, setData] = useState(originData);
    const [editingKey, setEditingKey] = useState('');
    const [addProduct, setAddProduct] = useState(false);

    const isEditing = (record) => record.key === editingKey;

    const edit = (record) => {
        form.setFieldsValue({
            name: '',
            age: '',
            address: '',
            ...record,
        });
        setEditingKey(record.key);
    };

    const cancel = () => {
        setEditingKey('');
    };

    const deleteProduct = () => {

    }

    const save = async (key) => {
        try {
            const row = await form.validateFields();
            const newData = [...data];
            const index = newData.findIndex((item) => key === item.key);

            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {...item, ...row});
                setData(newData);
                setEditingKey('');
            } else {
                newData.push(row);
                setData(newData);
                setEditingKey('');
            }
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };

    const columns = [
        {
            title: 'name',
            dataIndex: 'name',
            width: '25%',
            editable: true,
        },
        {
            title: 'age',
            dataIndex: 'age',
            width: '15%',
            editable: true,
        },
        {
            title: 'address',
            dataIndex: 'address',
            width: '40%',
            editable: true,
        },
        {
            title: 'Sửa',
            dataIndex: 'repair',
            render: (_, record) => {
                const editable = isEditing(record);
                return editable ? (
                    <span>
            <a
                href="javascript:;"
                onClick={() => save(record.key)}
                style={{
                    marginRight: 8,
                }}
            >
              Lưu
            </a>
            <Popconfirm title="Xác nhân hủy?" onConfirm={cancel}>
              <a>Đóng</a>
            </Popconfirm>
          </span>
                ) : (
                    <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
                        Sửa
                    </Typography.Link>
                );
            },
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            render: (_, record) => {
                const editable = isEditing(record);
                return (
                    <Popconfirm title="Xác nhân xóa?" onConfirm={deleteProduct}>
                        <a>Xóa</a>
                    </Popconfirm>
                );
            },
        },
    ];

    const mergedColumns = columns.map((col) => {
        if (!col.editable) {
            return col;
        }

        return {
            ...col,
            onCell: (record) => ({
                record,
                inputType: col.dataIndex === 'age' ? 'number' : 'text',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isEditing(record),
            }),
        };
    });

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    React.useEffect(()=>{
        console.log(props.data)
    },[props.data])

    return (
        <div>
            <div className={classes.header_1}>
                <span className={classes.title}>Danh sách sản phẩm</span>
                <Button variant="contained" color="primary" onClick={() => setAddProduct(true)}>
                    <FontAwesomeIcon icon={faPlusCircle} className={classes.iconAdd}></FontAwesomeIcon>
                    <span>Thêm sản phẩm</span>
                </Button>
            </div>
            <div className={'listProduct'}>
                <Form form={form} component={false}>
                    <Table components={{
                        body: {
                            cell: EditableCell,
                        },
                    }}
                           bordered
                           dataSource={data}
                           columns={mergedColumns}
                           rowClassName="editable-row"
                           pagination={{
                               onChange: cancel,
                           }}/>
                </Form>
            </div>
            <Modal
                title="Thêm sản phẩm"
                centered
                visible={addProduct}
                onOk={() => setAddProduct(false)}
                onCancel={() => setAddProduct(false)}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="addProduct" key="submit" htmlType="submit" type={'primary'}>
                            Submit
                        </Button>
                    </Form.Item>
                ]}
            >
                <Form
                    {...layout}
                    name="basic"
                    id={'addProduct'}
                    initialValues={{remember: true}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="Ảnh của sản phẩm"
                        name="image_product"
                        rules={[{required: true, message: 'Nhập đường dẫn ảnh vào đây!'}]}
                    >
                        <Input/>
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[{required: true, message: 'Please input your password!'}]}
                    >
                        <Input.Password/>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
