import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons";
import Button from "@material-ui/core/Button";
import {Form, Input, InputNumber, Popconfirm, Table, Typography} from "antd";
import Modal from "antd/es/modal/Modal";

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardCategoryBlack: {
        "&,& a,& a:hover,& a:focus": {
            color: 'black',
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    iconAdd: {
        marginRight: 4,
        fontSize: 18
    }
};

const useStyles = makeStyles(styles);


const EditableCell = ({
                          editing,
                          dataIndex,
                          title,
                          inputType,
                          record,
                          index,
                          children,
                          ...restProps
                      }) => {
    const inputNode = inputType === 'number' ? <InputNumber/> : <Input/>;
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{
                        margin: 0,
                    }}
                    rules={[
                        {
                            required: true,
                            message: `Please Input ${title}!`,
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                children
            )}
        </td>
    );
};

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

const originData = [
    {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
        tags: ['nice', 'developer'],
    },
    {
        key: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Joe Black',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
        tags: ['cool', 'teacher'],
    },
];

export default function Auctions(props) {
    const classes = useStyles();

    const [form] = Form.useForm();
    const [data, setData] = useState(originData);
    const [editingKey, setEditingKey] = useState('');
    const [addAuction, setAddAuction] = useState(false);

    const isEditing = (record) => record.key === editingKey;

    const edit = (record) => {
        form.setFieldsValue({
            name: '',
            age: '',
            address: '',
            ...record,
        });
        setEditingKey(record.key);
    };

    const cancel = () => {
        setEditingKey('');
    };

    const deleteProduct = () => {

    }

    const save = async (key) => {
        try {
            const row = await form.validateFields();
            const newData = [...data];
            const index = newData.findIndex((item) => key === item.key);

            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {...item, ...row});
                setData(newData);
                setEditingKey('');
            } else {
                newData.push(row);
                setData(newData);
                setEditingKey('');
            }
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };

    const columns = [
        {
            title: 'name',
            dataIndex: 'name',
            width: '25%',
            editable: true,
        },
        {
            title: 'age',
            dataIndex: 'age',
            width: '15%',
            editable: true,
        },
        {
            title: 'address',
            dataIndex: 'address',
            width: '40%',
            editable: true,
        },
        {
            title: 'Sửa',
            dataIndex: 'repair',
            render: (_, record) => {
                const editable = isEditing(record);
                return editable ? (
                    <span>
            <a
                href="javascript:;"
                onClick={() => save(record.key)}
                style={{
                    marginRight: 8,
                }}
            >
              Lưu
            </a>
            <Popconfirm title="Xác nhân hủy?" onConfirm={cancel}>
              <a>Đóng</a>
            </Popconfirm>
          </span>
                ) : (
                    <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
                        Sửa
                    </Typography.Link>
                );
            },
        },
    ];

    const mergedColumns = columns.map((col) => {
        if (!col.editable) {
            return col;
        }

        return {
            ...col,
            onCell: (record) => ({
                record,
                inputType: col.dataIndex === 'age' ? 'number' : 'text',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isEditing(record),
            }),
        };
    });

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    React.useEffect(() => {
        console.log(props.data)
    }, [props.data])

    return (
        <>
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="info">
                            <div className={classes.cardTitle}>
                                <div>
                                    <h3 className={classes.cardTitleWhite}>Quản lý phiên đấu giá</h3>
                                    <p className={classes.cardCategoryBlack}>
                                        Danh sách phiên đấu giá bao gồm n phiên đấu giá
                                    </p>
                                </div>
                                <Button variant="contained" color="default" onClick={() => {
                                    setAddAuction(true)
                                }}>
                                    <FontAwesomeIcon icon={faPlusCircle} className={classes.iconAdd}></FontAwesomeIcon>
                                    <span>Thêm phiên đấu giá</span>
                                </Button>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <Form form={form} component={false}>
                                <Table components={{
                                    body: {
                                        cell: EditableCell,
                                    },
                                }}
                                       bordered
                                       dataSource={data}
                                       columns={mergedColumns}
                                       rowClassName="editable-row"
                                       pagination={{
                                           onChange: cancel,
                                       }}/>
                            </Form>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
            <Modal
                title="Thêm phiên đấu giá"
                centered
                visible={addAuction}
                onOk={() => setAddAuction(false)}
                onCancel={() => setAddAuction(false)}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="addAuction" key="submit" htmlType="submit" type={'primary'}>
                            Submit
                        </Button>
                    </Form.Item>
                ]}
            >
                <Form
                    {...layout}
                    name="basic"
                    id={'addAuction'}
                    initialValues={{remember: true}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="Thời gian đấu giá"
                        name="time_auction"
                        rules={[{required: true, message: 'Nhập thời gian đấu giá vào đây!'}]}
                    >
                        <Input/>
                    </Form.Item>

                    <Form.Item
                        label="Nhập mã sản phẩm"
                        name="product_id"
                        rules={[{required: true, message: 'Nhập mã sản phẩm vào đây!'}]}
                    >
                        <Input.Password/>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
}
