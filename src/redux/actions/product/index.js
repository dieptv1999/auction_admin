import {
    FETCH_PAGINATE_PRODUCT,
    FETCH_PAGINATE_PRODUCT_SUCCEED,
    FETCH_PAGINATE_PRODUCT_FAILED,
    CREATE_PRODUCT,
    CREATE_PRODUCT_PRODUCT_SUCCEED,
    CREATE_PRODUCT_PRODUCT_FAILED,
    UPDATE_PRODUCT,
    UPDATE_PRODUCT_PRODUCT_SUCCEED,
    UPDATE_PRODUCT_PRODUCT_FAILED,
    DELETE_PRODUCT, DELETE_PRODUCT_PRODUCT_SUCCEED, DELETE_PRODUCT_PRODUCT_FAILED
} from './action_types';

export default {

    fetchPaginateProduct: (params, callback) => ({
        type: FETCH_PAGINATE_PRODUCT,
        params,
        callback,
    }),
    fetchPaginateProductSucceed: (data) => ({
        type: FETCH_PAGINATE_PRODUCT_SUCCEED,
        data,
    }),
    fetchPaginateProductFailed: (err) => ({
        type: FETCH_PAGINATE_PRODUCT_FAILED,
        err,
    }),

    createProduct: (params, callback) => ({
        type: CREATE_PRODUCT,
        params,
        callback,
    }),
    createProductSucceed: (data) => ({
        type: CREATE_PRODUCT_PRODUCT_SUCCEED,
        data,
    }),
    createProductFailed: (err) => ({
        type: CREATE_PRODUCT_PRODUCT_FAILED,
        err,
    }),

    updateProduct: (params, callback) => ({
        type: UPDATE_PRODUCT,
        params,
        callback,
    }),
    updateProductSucceed: (data) => ({
        type: UPDATE_PRODUCT_PRODUCT_SUCCEED,
        data,
    }),
    updateProductFailed: (err) => ({
        type: UPDATE_PRODUCT_PRODUCT_FAILED,
        err,
    }),

    deleteProduct: (params, callback) => ({
        type: DELETE_PRODUCT,
        params,
        callback,
    }),
    deleteProductSucceed: (data) => ({
        type: DELETE_PRODUCT_PRODUCT_SUCCEED,
        data,
    }),
    deleteProductFailed: (err) => ({
        type: DELETE_PRODUCT_PRODUCT_FAILED,
        err,
    }),
};
