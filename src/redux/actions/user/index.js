import {
    // FETCH_QR_CODE,
    // FETCH_QR_CODE_FAILED,
    // FETCH_QR_CODE_SUCCEED,
    LOGIN_HISTORY,
    LOGIN_HISTORY_FAILED,
    LOGIN_HISTORY_SUCCEED,
    USER_LIST,
    USER_LIST_SUCCEED,
    USER_LIST_FAILED,
    USER_BALANCES,
    USER_BALANCES_SUCCEED,
    USER_BALANCES_FAILED,
    USER_INFO,
    USER_INFO_SUCCEED,
    USER_INFO_FAILED,
    USER_LOGIN_HISTORY,
    USER_LOGIN_HISTORY_SUCCEED,
    USER_LOGIN_HISTORY_FAILED,
    USER_LIST_ADDRESS,
    USER_LIST_ADDRESS_SUCCEED,
    USER_LIST_ADDRESS_FAILED,
    USER_SYSTEM_LIST,
    USER_SYSTEM_LIST_SUCCEED,
    USER_SYSTEM_LIST_FAILED,
    USER_SYSTEM_INFO,
    USER_SYSTEM_INFO_SUCCEED,
    USER_SYSTEM_INFO_FAILED,
    LOGOUT,
    LOGOUT_SUCCEED,
    LOGOUT_FAILED,
    LOCK_USER,
    LOCK_USER_SUCCEED,
    LOCK_USER_FAILED,
    UNLOCK_USER,
    UNLOCK_USER_SUCCEED,
    UNLOCK_USER_FAILED, LOGIN_NORMAL, LOGIN_NORMAL_SUCCEED, LOGIN_NORMAL_FAILED,
} from './action_types';

export default {

    loginHistoryAction: (params, callback) => ({
        type: LOGIN_HISTORY,
        params,
        callback,
    }),
    loginHistoryActionSucceed: (data) => ({
        type: LOGIN_HISTORY_SUCCEED,
        data,
    }),
    loginHistoryActionFailed: (err) => ({
        type: LOGIN_HISTORY_FAILED,
        err,
    }),

    loginNormalAction: (params, callback) => ({
        type: LOGIN_NORMAL,
        params,
        callback,
    }),
    loginNormalActionSucceed: (data) => ({
        type: LOGIN_NORMAL_SUCCEED,
        data,
    }),
    loginNormalActionFailed: (err) => ({
        type: LOGIN_NORMAL_FAILED,
        err,
    }),

    userBalancesAction: (params) => ({
        type: USER_BALANCES,
        params,
    }),
    userBalancesActionSucceed: (data) => ({
        type: USER_BALANCES_SUCCEED,
        data,
    }),
    userBalancesActionFailed: (err) => ({
        type: USER_BALANCES_FAILED,
        err,
    }),

    userListAction: (params, callback) => ({
        type: USER_LIST,
        params,
        callback,
    }),
    userListActionSucceed: (data) => ({
        type: USER_LIST_SUCCEED,
        data,
    }),
    userListActionFailed: (err) => ({
        type: USER_LIST_FAILED,
        err,
    }),

    logout: (data) => ({
        type: LOGOUT,
        params: {
            data,
        },
    }),
    logoutSucceed: (data) => ({
        type: LOGOUT_SUCCEED,
        params: {
            data,
        },
    }),
    logoutFailed: (err) => ({
        type: LOGOUT_FAILED,
        err,
    }),

    userInfoAction: (params) => ({
        type: USER_INFO,
        params,
    }),
    userInfoActionSucceed: (data) => ({
        type: USER_INFO_SUCCEED,
        data,
    }),
    userInfoActionFailed: (err) => ({
        type: USER_INFO_FAILED,
        err,
    }),

    userLoginHitoryAction: (params, callback) => ({
        type: USER_LOGIN_HISTORY,
        params,
        callback,
    }),
    userLoginHitoryActionSucceed: (data) => ({
        type: USER_LOGIN_HISTORY_SUCCEED,
        data,
    }),
    userLoginHitoryActionFailed: (err) => ({
        type: USER_LOGIN_HISTORY_FAILED,
        err,
    }),

    userListAddressAction: (params, callback) => ({
        type: USER_LIST_ADDRESS,
        params,
        callback,
    }),
    userListAddressActionSucceed: (data) => ({
        type: USER_LIST_ADDRESS_SUCCEED,
        data,
    }),
    userListAddressActionFailed: (err) => ({
        type: USER_LIST_ADDRESS_FAILED,
        err,
    }),

    userSystemListAction: (params, callback) => ({
        type: USER_SYSTEM_LIST,
        params,
        callback,
    }),
    userSystemListActionSucceed: (data) => ({
        type: USER_SYSTEM_LIST_SUCCEED,
        data,
    }),
    userSystemListActionFailed: (err) => ({
        type: USER_SYSTEM_LIST_FAILED,
        err,
    }),

    userSystemInfoAction: (params, callback) => ({
        type: USER_SYSTEM_INFO,
        params,
        callback,
    }),
    userSystemInfoActionSucceed: (data) => ({
        type: USER_SYSTEM_INFO_SUCCEED,
        data,
    }),
    userSystemInfoActionFailed: (err) => ({
        type: USER_SYSTEM_INFO_FAILED,
        err,
    }),
    lockUser: (params, callback) => ({
        type: LOCK_USER,
        params,
        callback,
    }),
    lockUserSucceed: (data) => ({
        type: LOCK_USER_SUCCEED,
        data,
    }),
    lockUserFailed: (err) => ({
        type: LOCK_USER_FAILED,
        err,
    }),
    unlockUser: (params, callback) => ({
        type: UNLOCK_USER,
        params,
        callback,
    }),
    unlockUserSucceed: (data) => ({
        type: UNLOCK_USER_SUCCEED,
        data,
    }),
    unlockUserFailed: (err) => ({
        type: UNLOCK_USER_FAILED,
        err,
    }),
};
