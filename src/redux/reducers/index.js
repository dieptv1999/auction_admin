import {combineReducers} from 'redux';
import user from './user_reducer';
import dashboard from './dashboard_reducer';
import product from './product_reducer';

const allReducers = combineReducers({
    user,
    dashboard,
    product,
});

export default allReducers;
