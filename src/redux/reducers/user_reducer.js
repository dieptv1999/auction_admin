import {
    LOGIN_HISTORY_SUCCEED,
    USER_LIST_SUCCEED,
    USER_BALANCES_SUCCEED,
    CLEAR_USER_DATA,
    USER_INFO_SUCCEED,
    USER_LOGIN_HISTORY_SUCCEED,
    USER_LIST_ADDRESS_SUCCEED,
    USER_SYSTEM_LIST_SUCCEED,
    USER_SYSTEM_INFO_SUCCEED,
    LOGIN_NORMAL_SUCCEED,
    LOGOUT_SUCCEED
} from '../actions/user/action_types';
import utils from '../../common/utils';

const defaultParams = {
    session: {},
    loginHistories: [],
    userBalances: [],
    isAuthenticated: false,
    userLoginHistory: [],
    userAddressList: [],
    userSystemList: [],
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case LOGIN_HISTORY_SUCCEED:
            // const loginHistories = window._.map(action.data, (loginHistory) => {
            //     loginHistory.key = loginHistory.created_at;
            //     loginHistory.created_at = utils.formatTimeFromUnix(loginHistory.created_at, 'DD/MM/YYYY HH:mm:ss');
            //     return loginHistory;
            // });
            // return {
            //     ...state,
            //     loginHistories,
            // };
        case LOGIN_NORMAL_SUCCEED:
            window.$isAuthenticated = true;
            return {
                ...state,
                session: action.params.data,
                isAuthenticated: true,
            };
        case USER_LIST_SUCCEED:
            // const users = window._.map(action.data, (user) => {
            //     user.key = user.created_at;
            //     user.registration_date = utils.formatTimeFromUnix(user.registration_date, 'DD/MM/YYYY HH:mm:ss');
            //     return user;
            // });
            // return {
            //     ...state,
            //     users,
            // };
        case LOGOUT_SUCCEED:
            return defaultParams;
        case CLEAR_USER_DATA:
            return defaultParams;
        case USER_BALANCES_SUCCEED:
            return {
                ...state
            }
        case USER_LOGIN_HISTORY_SUCCEED:
            return {
                ...state,
                userLoginHistory: action.data,
            };
        case USER_LIST_ADDRESS_SUCCEED:
            return {
                ...state,
                userAddressList: action.data,
            };
        case USER_SYSTEM_LIST_SUCCEED:
            return {
                ...state,
                userSystemList: action.data,
            };
        case USER_SYSTEM_INFO_SUCCEED:
            return {
                ...state,
                userSystemInfo: action.data,
            };
        default:
            return {
                ...state,
            };
    }
};
