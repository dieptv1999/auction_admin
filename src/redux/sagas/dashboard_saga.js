import {
    all, call, fork, put, select, takeLatest,
} from 'redux-saga/effects';

function* watchDashBoard() {
}

export default function* rootSaga() {
    yield all([fork(watchDashBoard)]);
}
