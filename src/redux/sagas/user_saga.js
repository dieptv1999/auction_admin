import {
    put, takeLatest, call, all, fork,
} from 'redux-saga/effects';

import actions from '../actions/user';
import rf from '../../requests/RequestFactory';
import _ from 'lodash';
import consts, {BASE_URL} from '../../consts';
import utils from '../../common/utils';
import {LOGIN_HISTORY, LOGIN_NORMAL, LOGOUT} from "../actions/user/action_types";
import moment from "moment";
import axios from "axios";

function* loginHistory(action) {
    console.log('=== loginQRStep1 ===');
    try {
        const resp = yield call((params) => rf.getRequest('LoginRequest').loginHistories(params), action.params);

        if (resp.error.code === 400) {
            utils.showNotification('Login', 'Error Step 1', consts.TYPE_ERROR);
            yield put(actions.loginHistoryActionFailed(new Error('Error Step 1')));
            return;
        }
        console.log(resp, 'resp');
        const {data} = resp;

        yield put(actions.loginHistoryActionSucceed(data));
        console.log(data, 'data =======');
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.loginHistoryActionFailed(err));
    }
}


function* checkAuthSession(action){
    const strSession = window.localStorage.getItem('session');
    const session = JSON.parse(strSession || '{}');
    // check token expired
    if (!!session && moment(session.time_expired / 1000).isAfter()) {
        // add token for axios
        window.axios = axios.create({
            baseURL: BASE_URL,
            headers: {
                token: session.token,
            },
        });
        if (_.isFunction(action.callback)) action.callback()
        yield put(actions.loginNormalActionSucceed(session));
    }
    yield put(actions.loginNormalActionFailed());
}

function* loginNormal(action) {
    console.log('=== loginNormal ===');
    try {
        const resp = yield call((params) => rf.getRequest('LoginRequest').loginNormal(params), action.params);

        if (resp.error.code === 400) {
            utils.showNotification('Login', 'Error Step 1', consts.TYPE_ERROR);
            yield put(actions.loginHistoryActionFailed(new Error('Error Step 1')));
            return;
        }
        console.log(resp, 'resp');
        const {data} = resp;
        window.axios = axios.create({
            baseURL: BASE_URL,
            headers: {
                token: data.token,
            },
        });
        yield window.localStorage.setItem('session', JSON.stringify(data));

        yield put(actions.loginNormalActionSucceed(data));
        console.log(data, 'data =======');
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.loginNormalActionFailed(err));
    }
}

function* logout(action) {
    try {
        const {data} = yield call((params) => rf.getRequest('LoginRequest').logout(params), action.params.data);
        yield put(actions.logoutSucceed(data));
        window.localStorage.setItem('session', '');
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.logoutFailed(err));
    }
}

function* watchAllUsers() {
    yield takeLatest(LOGIN_HISTORY, checkAuthSession);
    yield takeLatest(LOGIN_NORMAL, loginNormal);
    yield takeLatest(LOGOUT, logout);
}

export default function* rootSaga() {
    yield all([fork(watchAllUsers)]);
}
