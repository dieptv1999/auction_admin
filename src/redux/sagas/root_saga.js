import {all} from 'redux-saga/effects';
import watchInit from './init_saga';
import watchDashBoard from './dashboard_saga'
import watchAllUser from './user_saga'

function* rootSaga() {
    yield all([
        watchInit(),
        watchDashBoard(),
        watchAllUser(),
    ]);
}

export default rootSaga;
