import {
  all, call, fork, put, takeLatest,
} from 'redux-saga/effects';

function* watchInit() {
}

export default function* rootSaga() {
  yield all([fork(watchInit)]);
}
